using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Linq;
using static System.Double;

namespace ConsoleApplication1
{
    public class PathFinder
    {
        private Dictionary<IntersectionNode, Node> nodesDict = new Dictionary<IntersectionNode, Node>();

        public void addIntersection(IntersectionNode i)
        {
            nodesDict.Add(i, new Node());
        }

        public List<IntersectionNode> findPath(IntersectionNode from, IntersectionNode to)
        {
            var currNode = from;
            nodesDict[currNode].Distance = 0;
            Debug.Assert(to != null, nameof(to) + " != null");
            do
            {
                nodesDict[currNode].IsVisited = true;

                var n = currNode.Neighbours;
                foreach (var intersectionNode in n)
                {
                    var node = nodesDict[intersectionNode];
                    node.Distance = Math.Min(node.Distance,
                        currNode.getLength(intersectionNode) + nodesDict[currNode].Distance);
                }

                var minNodePair = (from x in n.Select(key => nodesDict[key]) where x.IsVisited == false select x).Min();
                var minIntersectionNode = nodesDict.First(pair => pair.Value == minNodePair).Key;
                currNode = minIntersectionNode;
                //TODO add exit if no path
            } while (currNode != to);

            var returnList = new List<IntersectionNode>();
            returnList.Add(to);
            currNode = to;
            while (currNode != from)
            {
                
                var n = currNode.Neighbours;
                
                var nextNode = (from x in n.Select(key => nodesDict[key]) where x.IsVisited == true select x).Min();
                var nextIntersectionNode = nodesDict.First(pair => pair.Value == nextNode).Key;
                
                returnList.Add(nextIntersectionNode);
                
                currNode = nextIntersectionNode;
            }

            returnList.Reverse();
            return returnList;    //TODO
        }
        private 

        class Node : IComparable<Node>
        {
            public double Distance { get; set; }
            public bool IsVisited { get; set; }

            public Node(double distance = PositiveInfinity, bool isVisited = false)
            {
                IsVisited = isVisited;
                Distance = distance;
            }

            public int CompareTo(Node other)
            {
                if (ReferenceEquals(this, other)) return 0;
                if (ReferenceEquals(null, other)) return 1;
                var distanceComparison = Distance.CompareTo(other.Distance);
                if (distanceComparison != 0) return distanceComparison;
                return IsVisited.CompareTo(other.IsVisited);
            }
        }
    }
}