using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    public class AlkashkaNode
    {
        public class Alkashka
        {
            public string Name1 { get; }
            public int Degree { get; }
            public AlkoType Type { get; }

            public Alkashka(string name, int degree, AlkoType type)
            {
                Name1 = name;
                this.Degree = degree;
                this.Type = type;
            }

            public enum AlkoType
            {
                Vodka,
                Beer,
                Ale,
                Cider,
                Other
            }
            
        }
        
        private string Name;
        public Dictionary<Alkashka, double> AlkoList { get; }

        public AlkashkaNode(string name)
        {
            Name = name;
        }
    }
}