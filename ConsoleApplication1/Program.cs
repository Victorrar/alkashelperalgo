﻿using System;
using System.Collections.Generic;
using static ConsoleApplication1.AlkashkaNode.Alkashka.AlkoType;


namespace ConsoleApplication1
{
    public struct EvenyaInfo

    {
        public double Lat { get; }
        public double Lng { get; }
        public int[] Neigbours { get; }

        public EvenyaInfo(double lat, double lng, int[] neigbours)
        {
            Lat = lat;
            Lng = lng;
            Neigbours = neigbours;
        }
    }

    internal class Program
    {
        private static EvenyaInfo[] initInfo =
        {
            new EvenyaInfo(100, 100, new[] {1, 2}),
            new EvenyaInfo(100, 200, new[] {0, 2}),
            new EvenyaInfo(200, 200, new[] {1, 0}),
        };

        private static PathFinder _pf;

        private static List<IntersectionNode> inList = new List<IntersectionNode>(100);

        public static void Main()
        {
            _pf = new PathFinder();
            FillData();

            var list = _pf.findPath(inList[0], inList[2]);

            Console.WriteLine(list);
        }

        private static void FillData()
        {
            foreach (var info in initInfo)
            {
                inList.Add(new IntersectionNode(info.Lat, info.Lng));
            }

            for (var i = 0; i < initInfo.Length; i++)
            {
                var evenyaInfo = initInfo[i];

                foreach (var neigbour in evenyaInfo.Neigbours)
                {
                    inList[i].Neighbours.Add(inList[neigbour]);
                }
            }

            foreach (var node in inList)
            {
                _pf.addIntersection(node);
            }
        }

        private static void FillAlkashka()
        {
            AlkashkaNode atb = new AlkashkaNode("atb");
            atb.AlkoList.Add(new AlkashkaNode.Alkashka("Nemiroff", 40, Vodka), 40);
            atb.AlkoList.Add(new AlkashkaNode.Alkashka("vozduh", 40, Vodka), 40);

            AlkashkaNode silpo = new AlkashkaNode("silpo");
            atb.AlkoList.Add(new AlkashkaNode.Alkashka("Nemiroff", 40, Vodka), 40);
            atb.AlkoList.Add(new AlkashkaNode.Alkashka("vozduh", 40, Vodka), 40);

            AlkashkaNode himiki = new AlkashkaNode("Obshaga himikov");
            atb.AlkoList.Add(new AlkashkaNode.Alkashka("KPI konyak", 146, Other), 1);
        }
    }
}