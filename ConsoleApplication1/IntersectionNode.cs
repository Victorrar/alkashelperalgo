using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace ConsoleApplication1
{
    public class IntersectionNode 
    {
        public double Lat { get; }
        public double Lng { get; }
        public double PathPenalty { get; }
        public AlkashkaNode AlkashkaNode { get; }
        public List<IntersectionNode> Neighbours { get; }


        public double getLength(IntersectionNode an)
        {
            var R = 6371e3; // metres
            var φ1 = DegreeToRadian(Lat);
            var φ2 = DegreeToRadian(an.Lat);
            var Δφ = DegreeToRadian(an.Lat - Lat);
            var Δλ = DegreeToRadian(an.Lng - Lng);

            var a = Math.Sin(Δφ / 2) * Math.Sin(Δφ / 2) +
                    Math.Cos(φ1) * Math.Cos(φ2) *
                    Math.Sin(Δλ / 2) * Math.Sin(Δλ / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            var d = R * c;
            
            return d + PathPenalty + an.PathPenalty;
        }

        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }


        public IntersectionNode(double lat, double lng, double pathPenalty = 0) : this(lat, lng, pathPenalty, new List<IntersectionNode>())
        {
        }

        public IntersectionNode(double lat, double lng, double pathPenalty, List<IntersectionNode> neighbours = null)
        {
            Lat = lat;
            Lng = lng;
            PathPenalty = pathPenalty;
            Neighbours = neighbours;
        }
    }
}